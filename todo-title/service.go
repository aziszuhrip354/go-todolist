package todotitle

import (
	"errors"
)

type Service interface {
	FindAll() ([]TodoTitle, error)
	FindByID(ID int) (TodoTitle, error)
	Create(todoTitle TodoTitle) (TodoTitle, error)
	FindByTitle(Title string) (TodoTitle, error)
	DeleteById(ID int) (TodoTitle, error)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository}
}

func (s *service) FindAll() ([]TodoTitle, error) {
	todoTitles, err := s.repository.FindAll()
	return todoTitles, err
}

func (s *service) FindByID(ID int) (TodoTitle, error) {
	todoTitle, err := s.repository.FindByID(ID)
	return todoTitle, err
}

func (s *service) Create(todoTitle TodoTitle) (TodoTitle, error) {
	newTitle := TodoTitle{
		TitleName: todoTitle.TitleName,
	}
	newTitle, err := s.repository.Create(newTitle)
	return newTitle, err
}

func (s *service) FindByTitle(Title string) (TodoTitle, error) {
	todoTitle, err := s.repository.FindByTitle(Title)
	return todoTitle, err
}

func (s *service) DeleteById(ID int) (TodoTitle, error) {
	var todoTitle TodoTitle
	todoTitle, err := s.repository.FindByID(ID)
	if err == nil {
		todoTitle, err = s.repository.DeleteById(todoTitle)
	} else {
		err = errors.New("data title is not found")
	}
	return todoTitle, err
}

func (s *service) UpdateById(ID int, Title string) (TodoTitle, error) {
	var todoTitle TodoTitle
	todoTitle, err := s.repository.FindByID(ID)
	if err == nil {
		todoTitle.TitleName = Title
		todoTitle, err = s.repository.UpdateById(todoTitle)
	} else {
		err = errors.New("data title is not found")
	}
	return todoTitle, err
}
