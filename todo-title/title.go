package todotitle

import "time"

type TodoTitle struct {
	ID        int       `json:"id"`
	TitleName string    `json:"todo_title" binding:"required"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
