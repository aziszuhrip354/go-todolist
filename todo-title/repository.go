package todotitle

import "gorm.io/gorm"

type Repository interface {
	FindAll() ([]TodoTitle, error)
	FindByID(ID int) (TodoTitle, error)
	Create(titleTodo TodoTitle) (TodoTitle, error)
	FindByTitle(Title string) (TodoTitle, error)
	DeleteById(todoTitle TodoTitle) (TodoTitle, error)
	UpdateById(TodoTitle TodoTitle) (TodoTitle, error)
}

type repository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) *repository {
	return &repository{db}
}

func (r *repository) FindAll() ([]TodoTitle, error) {
	var todoTitles []TodoTitle
	err := r.db.Find(&todoTitles).Error

	return todoTitles, err
}

func (r *repository) FindByID(ID int) (TodoTitle, error) {
	var todoTitle TodoTitle
	err := r.db.First(&todoTitle, ID).Error
	return todoTitle, err
}

func (r *repository) Create(titleTodo TodoTitle) (TodoTitle, error) {
	err := r.db.Create(&titleTodo).Error
	return titleTodo, err
}

func (r *repository) FindByTitle(titleNamte string) (TodoTitle, error) {
	var todoTitle TodoTitle
	err := r.db.Where("title_name LIKE ? ", titleNamte).First(&todoTitle).Error
	return todoTitle, err
}

func (r *repository) DeleteById(todoTitle TodoTitle) (TodoTitle, error) {
	err := r.db.Delete(&todoTitle).Error
	return todoTitle, err
}

func (r *repository) UpdateById(todoTitle TodoTitle) (TodoTitle, error) {
	err := r.db.Save(&todoTitle).Error
	return todoTitle, err
}
