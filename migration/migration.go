package migration

import (
	"fmt"
	"log"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func AutoMigrate() {
	dsn := "root:root@tcp(localhost:3306)/todolist?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal("=============== DB CONNECTION ERROR ========================")
	}

	db.AutoMigrate(
		&TodoTitle{},
		&Todo{},
	)
	fmt.Println("MIGRATION SUCCEED")
}
