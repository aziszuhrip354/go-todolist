package migration

import "time"

type Todo struct {
	ID        int       `gorm:"primaryKey,autoIncrement"`
	TitleID   int       `gorm:"NOT NULL"`
	TodoName  string    `gorm:"NOT NULL"`
	Deadline  time.Time `gorm:"NOT NULL"`
	Status    string    `gorm:"NOT NULL"`
	CreatedAt time.Time `gorm:"NOT NULL"`
	UpdatedAt time.Time `gorm:"NOT NULL"`
}
