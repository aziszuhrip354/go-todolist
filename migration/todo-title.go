package migration

import "time"

type TodoTitle struct {
	ID        int       `gorm:"primaryKey,autoIncrement"`
	TitleName string    `gorm:"NOT NULL"`
	CreatedAt time.Time `gorm:"NOT NULL"`
	UpdatedAt time.Time `gorm:"NOT NULL"`
}
