package main

import (
	"go-todolist/handler"
	"go-todolist/migration"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func main() {

	// Auto Migration
	migration.AutoMigrate()

	router := gin.Default()
	r := router

	corsConfig := cors.DefaultConfig()

	corsConfig.AllowOrigins = []string{"*"}
	// To be able to send tokens to the server.
	corsConfig.AllowCredentials = true

	// OPTIONS method for ReactJS
	corsConfig.AddAllowMethods("OPTIONS")

	// Register the middleware
	router.Use(cors.New(corsConfig))

	v1 := r.Group("v1")

	rTitleTodo := v1.Group("title-todo")
	rTitleTodo.GET("data/all", handler.AllTitleTodoGET)
	rTitleTodo.POST("data", handler.TitleTodoPOST)
	rTitleTodo.DELETE("data/:id", handler.TitleTodoDelete)
	rTitleTodo.PUT("data/:id", handler.TitleTodoUpdate)

	rTodoList := v1.Group("todolist")
	rTodoList.GET("data", handler.TodoGET)
	rTodoList.GET("data/:id/all", handler.TodoAllGET)
	rTodoList.POST("data", handler.TodoPOST)

	r.Run(":3000")
}
