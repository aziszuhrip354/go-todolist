package handler

import (
	"go-todolist/todo"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func TodoGET(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"status":  true,
		"message": "Success get data todolist",
	})
}

func TodoAllGET(c *gin.Context) {
	stringTitleId := c.Param("id")
	titleId, _ := strconv.Atoi(stringTitleId)

	repository := todo.NewRepository(db)
	service := todo.NewService(repository)
	getAllTodo, totalData, err := service.FindAllTodoByTitleId(int(titleId))
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"status":  false,
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"status":     true,
		"data":       getAllTodo,
		"total_data": totalData,
		"message":    "Data successfully to get",
	})
}

func TodoPOST(c *gin.Context) {
	var request todo.Todo
	err := c.ShouldBindJSON(&request)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"status":  false,
			"message": err.Error(),
		})
		return
	}

	repository := todo.NewRepository(db)
	service := todo.NewService(repository)

	newTodo := todo.Todo{
		TitleID:  request.TitleID,
		TodoName: request.TodoName,
		Deadline: request.Deadline,
		Status:   request.Status,
	}

	_, err = service.CraeteTodo(newTodo)
	var lastTodo todo.Todo
	lastTodo, _ = service.FindLastTodo(lastTodo)

	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"status":  false,
			"message": err.Error(),
		})
		return
	}

	newTodo.ID = lastTodo.ID

	c.JSON(http.StatusOK, gin.H{
		"status":  true,
		"message": "Successfully created new todo",
		"data":    newTodo,
	})
}
