package handler

import (
	todotitle "go-todolist/todo-title"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var dsn = "root:root@tcp(localhost:3306)/todolist?charset=utf8mb4&parseTime=True&loc=Local"
var db, errDb = gorm.Open(mysql.Open(dsn), &gorm.Config{})

func AllTitleTodoGET(c *gin.Context) {
	repository := todotitle.NewRepository(db)
	service := todotitle.NewService(repository)

	getTitles, err := service.FindAll()

	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"status":  true,
			"message": "No data exist",
		})
	}

	c.JSON(http.StatusOK, gin.H{
		"status":  true,
		"message": "Success to get data title todo",
		"data":    getTitles,
	})
}

func TitleTodoPOST(c *gin.Context) {
	var request todotitle.TodoTitle
	err := c.ShouldBindJSON(&request)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  false,
			"message": err.Error(), //"Data title is required, err: " + err.Error(),
		})
		return
	}

	repository := todotitle.NewRepository(db)
	service := todotitle.NewService(repository)

	_, err = service.FindByTitle(request.TitleName)
	if err == nil { // if title was created
		c.JSON(http.StatusOK, gin.H{
			"status":  false,
			"message": "Title was created",
		})
		return
	}

	newTitle := todotitle.TodoTitle{
		TitleName: request.TitleName,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	_, err = service.Create(newTitle)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  false,
			"message": "Failed to create data: " + err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"status":  true,
		"message": "Success to saved new todo title",
		"data":    newTitle,
		"id":      newTitle.ID,
	})
}

func TitleTodoDelete(c *gin.Context) {
	stringId := c.Param("id")
	titleId, _ := strconv.Atoi(stringId)

	repository := todotitle.NewRepository(db)
	service := todotitle.NewService(repository)
	_, err := service.DeleteById(int(titleId))
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"status":  false,
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"status":  true,
		"message": "Success to deleted data title",
	})
}

func TitleTodoUpdate(c *gin.Context) {
	var request todotitle.TodoTitle
	err := c.ShouldBindJSON(&request)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"status":  false,
			"message": "Maaf.. title must be fill",
		})
		return
	}

	stringId := c.Param("id")

	titleId, _ := strconv.Atoi(stringId)

	repository := todotitle.NewRepository(db)
	service := todotitle.NewService(repository)
	_, err = service.UpdateById(int(titleId), request.TitleName)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"status":  false,
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"status":  true,
		"message": "Data title success updated",
	})
}
