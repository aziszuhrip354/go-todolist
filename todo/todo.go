package todo

import "time"

type Todo struct {
	ID        int       `json:"id"`
	TitleID   int       `json:"title_id" binding:"required"`
	TodoName  string    `json:"todo_name" binding:"required"`
	Deadline  string    `json:"deadline" bindinng:"required"`
	Status    string    `json:"status" binding:"required"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
