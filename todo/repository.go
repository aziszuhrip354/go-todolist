package todo

import (
	"errors"

	"gorm.io/gorm"
)

type Repository interface {
	FindAllTodoByTitleId(ID int) ([]Todo, int64, error)
	CraeteTodo(todo Todo) (Todo, error)
	findLastTodo(todo Todo) (Todo, error)
}

type repository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) *repository {
	return &repository{db}
}

func (r *repository) FindAllTodoByTitleId(ID int) ([]Todo, int64, error) {
	var todolist []Todo
	var totalData int64 = 10

	err := r.db.Where("title_id = ?", ID).Order("deadline asc").Find(&todolist).Count(&totalData).Error
	if totalData == 0 {
		err = errors.New("data todo not found")
	}
	return todolist, totalData, err
}

func (r *repository) CraeteTodo(todo Todo) (Todo, error) {
	err := r.db.Create(&todo).Error
	return todo, err
}

func (r *repository) findLastTodo(todo Todo) (Todo, error) {
	err := r.db.Last(&todo).Error
	return todo, err
}
