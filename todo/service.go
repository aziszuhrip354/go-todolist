package todo

import (
	"errors"
	"time"
)

type Service interface {
	FindAllTodoByTitleId(ID int) ([]Todo, int64, error)
	CraeteTodo(todo Todo) (Todo, error)
	findLastTodo(todo Todo) (Todo, error)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository}
}

func (s *service) FindAllTodoByTitleId(ID int) ([]Todo, int64, error) {
	todos, totalData, err := s.repository.FindAllTodoByTitleId(ID)
	if err != nil {
		err = errors.New("data todo is not exist")
	}

	return todos, totalData, err
}

func (s *service) CraeteTodo(todo Todo) (Todo, error) {
	newTodo := Todo{
		TitleID:   todo.TitleID,
		TodoName:  todo.TodoName,
		Deadline:  todo.Deadline,
		Status:    todo.Status,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	newTodo, err := s.repository.CraeteTodo(newTodo)
	return todo, err
}

func (s *service) FindLastTodo(todo Todo) (Todo, error) {
	todo, err := s.repository.findLastTodo(todo)
	if err != nil {
		err = errors.New("no data exist")
	}
	return todo, err
}
